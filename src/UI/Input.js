import React, {Component} from 'react';
import {connect} from 'react-redux';

class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cityes: this.props.cityes,
            findedCityes: []
        }
    }

    inputChangeHandler = event => {
        if(!this.props.isLocation) {
            return;
        }

        const cityes = this.state.cityes;
        let findedCityes = [];
        const button = document.getElementById('popup__disabled');
        button.className = 'popup__button popup__disabled';
        
        for (let i = 0; i < cityes.length; i++) {
            if (cityes[i].toLowerCase().includes(event.target.value.toLowerCase())) {
                findedCityes.push(cityes[i]);
            }

            if (cityes[i].toLowerCase() === event.target.value.toLowerCase()) {
                event.target.value = cityes[i];
                button.className = 'popup__button popup__accept';
                findedCityes = [];
            }
        }

        if (event.target.value.length === 0) {
            findedCityes = [];
        }

        this.setState({
            findedCityes
        })
    }

    chooseCityHandler = city => {
        const input = document.getElementById('popup__input-location');
        const button = document.getElementById('popup__disabled');
        let findedCityes = [...this.state.findedCityes];
        const location = document.getElementsByClassName('header__city');

        input.value = city;
        button.className = 'popup__button popup__accept';
        findedCityes = [];
        location[0].textContent = city;
        location[1].textContent = city;

        this.setState({
            findedCityes
        })

        this.props.onChangeLocation(city);
    }

    fillFindedCitiesHandler = () => {
        const input = document.getElementById('popup__input-location');
        const cityes = [...this.state.cityes];
        const findedCityes = [];

        if (input.value.length < 1) {
            for (let i = 0; i < cityes.length; i++) {
                findedCityes.push(cityes[i])
            }
        } else {
            for (let i = 0; i < cityes.length; i++) {
                if (cityes[i].toLowerCase().includes(input.value.toLowerCase())) {
                    findedCityes.push(cityes[i]);
                }
            }
        }

        this.setState({
            findedCityes
        })
    }

    clearFindedCitiesHandler = () => {
        const findedCityes = [];

        this.setState({
            findedCityes
        })
    }

    render() {
        const cityes = this.state.findedCityes.map((city, index) => {
            return (
                <span
                    className="input__city"
                    key={index}
                    onClick={() => this.chooseCityHandler(city)}
                >{city}</span>
            )
        })
        return (
            this.props.isLocation
                ? <div className="input" style={{width: '100%', display: 'flex', justifyContent: 'center'}}>
                    <input
                        type={this.props.type}
                        placeholder={this.props.placeholder}
                        className={this.props.className}
                        id={'popup__input-location'}
                        onChange={this.inputChangeHandler.bind(this)}
                        autoComplete="off"
                    />
                    {
                        this.state.findedCityes.length > 0
                            ? <div 
                                className="popup__arrow-container"
                                onClick={() => this.clearFindedCitiesHandler()}
                            >
                                <svg
                                    className="popup__arrow-up"
                                    width="22"
                                    height="12"
                                    viewBox="0 0 22 12"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path d="M1.2069 2.2069C1.06896 2.06897 1 1.89655 1 1.7069C1 1.51724 1.06896 1.34483 1.2069 1.2069C1.48276 0.931034 1.93103 0.931034 2.2069 1.2069L11 10L19.7931 1.2069C20.069 0.931034 20.5172 0.931034 20.7931 1.2069C21.069 1.48276 21.069 1.93103 20.7931 2.2069L11.5 11.5C11.2241 11.7759 10.7759 11.7759 10.5 11.5L1.2069 2.2069Z" fill="#171822" stroke="#171822" strokeWidth="0.2"/>
                                </svg>
                            </div>
                            : <div
                                className="popup__arrow-container"
                                onClick={() => this.fillFindedCitiesHandler()}
                            >
                                <svg
                                    className="popup__arrow"
                                    width="22"
                                    height="12"
                                    viewBox="0 0 22 12"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path d="M1.2069 2.2069C1.06896 2.06897 1 1.89655 1 1.7069C1 1.51724 1.06896 1.34483 1.2069 1.2069C1.48276 0.931034 1.93103 0.931034 2.2069 1.2069L11 10L19.7931 1.2069C20.069 0.931034 20.5172 0.931034 20.7931 1.2069C21.069 1.48276 21.069 1.93103 20.7931 2.2069L11.5 11.5C11.2241 11.7759 10.7759 11.7759 10.5 11.5L1.2069 2.2069Z" fill="#171822" stroke="#171822" strokeWidth="0.2"/>
                                </svg>
                            </div>
                    }
                    {
                        this.state.findedCityes.length > 0
                            ? <div className="input__hint-container-active">
                                <div className="input__hint-active">
                                    {cityes}
                                </div>
                            </div>
                            : <div className="input__hint-container-hidden">
                                <div className="input__hint-hidden">
                                </div>
                            </div>
                    }
                </div>
                : <input
                    type={this.props.type}
                    placeholder={this.props.placeholder}
                    className={this.props.className}
                    id={this.props.id}
                    autoComplete="off"
                />
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onChangeLocation: location => dispatch({type: 'CHANGE_CITY', payload: location}),
    }
}

export default connect(null, mapDispatchToProps)(Input);