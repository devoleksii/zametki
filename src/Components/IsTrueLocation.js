import React from 'react';

const showPopupHandler = () => {
    const popup = document.getElementById('popup popup-header');
    const bgLayer = document.getElementById('main__bg-layer');

    popup.className = 'popup';
    bgLayer.className = 'main__bg-layer';
}

document.addEventListener('click', event => {
    const modal = document.getElementById('is-true-location');
    
    if (event.target !== modal){
        modal.style.display = 'none';
    }
})

window.onload = () => {
    const modal = document.getElementById('is-true-location');

    modal.style.display = 'flex';
}

const IsTrueLocation = () => {
    return (
        <div
            id="is-true-location"
            className="is-true-location"
        >
            <svg className="is-true-location__rect" width="235" height="57" viewBox="0 0 235 57" fill="white" xmlns="http://www.w3.org/2000/svg">
                <rect x="199" width="20.0416" height="20.0416" rx="2" transform="rotate(45 199 0)" fill="white"/>
            </svg>
            <span className="is-true-location__question">Чи вірне це місцезнаходження?</span>
            <span
                className="is-true-location__link"
                onClick={showPopupHandler}
            >обрати</span>
        </div>
    )
}

export default IsTrueLocation;