import React, {Component} from 'react';

class PopupShare extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    hidePopupHandler = () => {
        const popup = document.getElementById(`popup-share${this.props.id}`);
        const bgLayer = document.getElementById('main__bg-layer');
        
        popup.className = 'popup-share-hidden';

        const activeModal = document.getElementsByClassName("popup");
        
        if (activeModal.length > 0) {
            return;
        }

        setTimeout(() => {
            bgLayer.className = 'main__bg-layer-hidden';
        }, 300);
    }

    copyTextHandler = () => {
        navigator.clipboard.writeText('https://www.google.com');

        this.hidePopupHandler();
    }

    render() {
        return (
            <div
                className="popup-share-none"
                id={`popup-share${this.props.id}`}
            >
                <a
                    onClick={this.hidePopupHandler.bind(this)}
                    className="popup-share__item"
                    target="blank"
                    href={`https://telegram.me/share/url?url=https://${"giveAway"}/${this.props.id}`}
                >Telegram</a>
                <a
                    onClick={this.hidePopupHandler.bind(this)}
                    className="popup-share__item"
                    href={`viber://forward?text=https://${"giveAway"}/${this.props.postId}`}
                >Viber</a>
                <a
                    onClick={this.hidePopupHandler.bind(this)}
                    className="popup-share__item"
                    href="https://www.google.com/"
                >Докладніше на сайті</a>
                <button
                    className="popup-share__item"
                    onClick={this.copyTextHandler.bind(this)}
                >Скопіювати лінк</button>
                <button
                    className="popup-share__item"
                    onClick={this.hidePopupHandler.bind(this)}
                >Скасувати</button>
            </div>
        )
    }
}

export default PopupShare;