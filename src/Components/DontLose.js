import React from 'react';
import Input from '../UI/Input';

const DontLose = props => {
    return (
        <div className="dont-lose">
            <div className="dont-lose__icon">
                <svg width="24" height="26" viewBox="0 0 24 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.9508 15.8683L16.7828 13.2921L21.4673 9.04833L12.0099 2.46411L2.55243 9.04833L7.23695 13.2921L10.06 15.8683L11.5952 14.4921C11.8337 14.2715 12.1949 14.2803 12.4245 14.4921L13.9508 15.8683ZM1.23789 8.55476L11.5863 1.16792V1.15903L11.5952 1.15013H11.6041L11.613 1.14124L11.6219 1.13235L11.6308 1.12346H11.6397L11.6486 1.11456L11.6575 1.10567L11.6664 1.09678H11.6752H11.6841V1.08844H11.693V1.07954H11.7108L11.7197 1.07065L11.7286 1.06176C11.7992 1.03508 11.8787 1.00895 11.9493 1.00006H11.9582H11.9671H11.976H11.9848H11.9937H12.0026H12.0115H12.0204H12.0293H12.0382H12.0471H12.056H12.0649C12.1444 1.00895 12.215 1.03508 12.2855 1.06176L12.2944 1.07065L12.3033 1.07954H12.3122H12.3211V1.08844H12.33L12.3389 1.09733H12.3478L12.3567 1.10622L12.3656 1.11512H12.3745L12.3834 1.12401L12.3923 1.1329L12.4012 1.1418H12.4101L12.4189 1.15069L12.4278 1.15958V1.16848L22.7763 8.55531C22.9263 8.66981 23.0147 8.84657 23.0147 9.04944V24.3736C23.0147 24.7177 22.7412 25.0001 22.3972 25.0001H12.0099H1.62642H1.60863L1.59085 24.9912H1.57306H1.56472H1.54694H1.52915L1.51136 24.9823H1.50247H1.48524L1.46745 24.9734H1.44967L1.44077 24.9645H1.42354L1.41465 24.9556C1.1762 24.8672 1 24.6382 1 24.3731V9.04889C1 8.84601 1.09671 8.66926 1.23789 8.55476ZM2.24394 10.4518V22.9707L6.39932 19.2033L9.14289 16.7065L6.39932 14.2186L2.24394 10.4518ZM21.7674 22.9707V10.4518L17.6209 14.2192L14.8685 16.7071L17.6209 19.2039L21.7674 22.9707ZM16.7828 20.121L12.0099 15.7894L7.23695 20.121L3.23165 23.7472H12.0099H20.7792L16.7828 20.121Z" fill="#171822" stroke="#171822" strokeWidth="0.2"/>
                </svg>
            </div>
            <div className="dont-lose__text">
                <h3 className="dont-lose__headliner">Не втрать обране!</h3>
                <p className="dont-lose__description">Підписавшись, Ви збережете свої піни.</p>
            </div>
            <form
                method="POST"
                action=""
                className="dont-lose__form">
                <Input
                    type={'email'}
                    placeholder={'email'}
                    className={'dont-lose__input'}
                />
                <button className="dont-lose__button" type="submit">Підписатися</button>
            </form>
        </div>
    )
}

export default DontLose;