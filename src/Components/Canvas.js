import React from 'react';

class Canvas extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            xPos: -50,
            yPos: 300,
            radius: 0,
            xChange: 1,
            yChange: 1,
            startAngle: (Math.PI / 180) * 0,
            endAngle: (Math.PI / 180) * 360
        };
    }

    drawCircle = () => {
        const canvas = document.getElementById("canvas");

        if (canvas) {
            var ctx = canvas.getContext('2d');
            var endXPos = canvas.width - 100;
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            let xPos = this.state.xPos;
            let yPos = this.state.yPos;
            let radius = this.state.radius;
            let xChange = this.state.xChange;
            let yChange = this.state.yChange;
            let startAngle = this.state.startAngle;
            let endAngle = this.state.endAngle;
    
            this.setState({
                xPos: xPos += xChange,
                yPos: yPos -= yChange,
                radius: radius += 5
            })
    
            if (xPos > endXPos) {
                this.setState({
                    xPos: -50,
                    yPos: 300,
                    radius: 0
                })
            };
            
            ctx.fillStyle = '#FDDB1A';
            ctx.beginPath();
            ctx.arc(xPos, yPos, radius, startAngle, endAngle, true);
            ctx.fill();
        
            ctx.fillStyle = '#23BDE4';
            ctx.beginPath();
            ctx.arc(xPos - 120, yPos + 120, radius, startAngle, endAngle, true);
            ctx.fill();
        
            ctx.fillStyle = '#E286F1';
            ctx.beginPath();
            ctx.arc(xPos - 220, yPos + 220, radius, startAngle, endAngle, true);
            ctx.fill();
    
            ctx.fillStyle = '#fff';
            ctx.beginPath();
            ctx.arc(xPos - 320, yPos + 320, radius, startAngle, endAngle, true);
            ctx.fill();
        }
    }

    componentDidMount() {
      const intervalDraw = setInterval(this.drawCircle, 10);
      this.setState({intervalDraw: intervalDraw});
    };
  
    componentWillUnmount(){
      clearInterval(this.state.intervalDraw);
    }

    render() {
        return (
            <canvas
                className="canvas"
                id="canvas"
            ></canvas>
        )
    }
}

export default Canvas;