import React, {Component} from 'react';
import Input from '../UI/Input';

class Popup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            valid: false
        }
    }

    showPopupHandler = () => {
        const popup = document.getElementById(`popup ${this.props.id}`);
        const bgLayer = document.getElementById('main__bg-layer');

        popup.className = 'popup';
        bgLayer.className = 'main__bg-layer';
    }

    hidePopupHandler = event => {
        const popup = document.getElementById(`popup ${this.props.id}`);
        const bgLayer = document.getElementById('main__bg-layer');
        const activeModal = document.getElementsByClassName("popup");

        if (event.target.className === 'popup__button popup__accept popup__disabled') {
            return;
        };
        
        popup.className = 'popup-hidden';

        if (activeModal.length > 0) {
            return;
        }

        setTimeout(() => {
            bgLayer.className = 'main__bg-layer-hidden';
        }, 300);
    }

    sendModalHandler = event => {
        event.preventDefault();

        const input = document.getElementById('popup__input');

        if (input.value.length === 0) {
            return;
        }

        this.hidePopupHandler();
    }

    componentDidMount() {
        if (this.props.main) {
            setTimeout(() => {
                this.showPopupHandler();
            }, 3000);
        }
    }

    render() {
        return (
            <div
                className={"popup-none"}
                id={`popup ${this.props.id}`}
            >
                <div className="popup__icon">
                    {this.props.logo}
                </div>
                <h3 className="popup__headliner">{this.props.title}</h3>
                <p className="popup__description">{this.props.description}</p>
                <form
                    method="POST"
                    action=""
                    className="popup__form"
                    onSubmit={this.sendModalHandler.bind(this)}
                >
                    <div className="popup__input-container">
                        <Input
                            type={this.props.type}
                            className={"popup__input"}
                            id={"popup__input"}
                            placeholder={this.props.placeHolder}
                            isLocation={this.props.isLocation}
                            cityes={this.props.cityes}
                        />
                    </div>
                    <div className="popup__buttons">
                        <button
                            type="submit"
                            className={`popup__button popup__accept ${this.props.isLocation ? 'popup__disabled' : null}`}
                            id="popup__disabled"
                            onClick={this.hidePopupHandler.bind(this)}
                        >{this.props.accept}</button>
                        <button
                            className="popup__button popup__decline"
                            onClick={this.hidePopupHandler.bind(this)}
                        >{this.props.decline}</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default Popup;