import React, {Component} from 'react';
import {connect} from 'react-redux';
import Logo from './Logo';
import Cover from './Cover';
import Popup from './Popup';
import IsTrueLocation from './IsTrueLocation';

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            city: {...this.props.city}
        }
    }

    showSearchFieldHandler = () => {
        const cover = document.getElementById('cover');
        const main = document.getElementById('main');
        const feedBack = document.getElementById('feedback');

        cover.className = 'cover-active';

        if (main) {
            setTimeout(() => {
                main.style.display = "none";
            }, 500);
        }

        if (feedBack) {
            setTimeout(() => {
                feedBack.style.display = "none";
            }, 500);
        }
    }

    showPopupHandler = () => {
        const popup = document.getElementById('popup popup-header');
        const bgLayer = document.getElementById('main__bg-layer');

        popup.className = 'popup';
        bgLayer.className = 'main__bg-layer';
    }

    render() {
        const popupIconLocation =
            <svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="60" height="60" rx="30" fill="white"/>
                <path d="M29.6919 18.0001C24.8992 18.0001 21 21.8992 21 26.6919C21 32.6398 28.7784 41.3716 29.1096 41.7405C29.4206 42.0869 29.9637 42.0863 30.2742 41.7405C30.6054 41.3716 38.3838 32.6398 38.3838 26.6919C38.3837 21.8992 34.4846 18.0001 29.6919 18.0001ZM29.6919 31.065C27.2806 31.065 25.3188 29.1033 25.3188 26.6919C25.3188 24.2806 27.2806 22.3189 29.6919 22.3189C32.1032 22.3189 34.0649 24.2806 34.0649 26.692C34.0649 29.1033 32.1032 31.065 29.6919 31.065Z" stroke="#171822" strokeWidth="1.4"/>
                <rect opacity="0.4" x="0.5" y="0.5" width="59" height="59" rx="29.5" stroke="#BFC8D6"/>
            </svg>;

        return (
            <header className={'header'}>
                <div className="header__container">
                    <Popup
                        logo={popupIconLocation}
                        title={'Де бачив рекламу?'}
                        placeHolder={'введіть локацію або місто'}
                        accept={'Обрати'}
                        decline={'Скасувати'}
                        select={true}
                        id="popup-header"
                        isLocation={true}
                        cityes={this.props.cityes}
                        city={this.props.city}
                    />
                    <Cover/>
                    <div>
                        <Logo className={'header__logo-container'}/>
                    </div>
                    <div>
                        <div
                            className={'header__location'}
                        >
                            <IsTrueLocation />
                            <div className={'header__city-container'}>
                                <span
                                    className={'header__city'}
                                    onClick={this.showPopupHandler}
                                >{this.state.city.city}</span>
                                <span
                                    onClick={this.showPopupHandler}
                                    className={'header__city'}
                                >{this.state.city.city}</span>
                            </div>
                            <svg width="16" height="22" viewBox="0 0 16 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M7.96758 0C3.57423 0 0 3.57423 0 7.96753C0 13.4198 7.1302 21.4239 7.43377 21.762C7.71892
                                    22.0796 8.21675 22.079 8.50138 21.762C8.80495 21.4239 15.9352 13.4198 15.9352 7.96753C15.9351
                                    3.57423 12.3609 0 7.96758 0ZM7.96758 11.9762C5.75717 11.9762 3.95893 10.1779 3.95893 7.96753C3.95893
                                    5.75713 5.75722 3.95889 7.96758 3.95889C10.1779 3.95889 11.9762 5.75717 11.9762 7.96758C11.9762 10.178
                                    10.1779 11.9762 7.96758 11.9762Z" fill="#171822"
                                />
                            </svg>
                        </div>
                        <div className={'header__search-container'}>
                            <svg
                                width="36"
                                height="36"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className='header__search-logo'
                                id='header__search-logo'
                                src='./img/search.png'
                                alt='search icon'
                                onClick={this.showSearchFieldHandler.bind(this)}
                            >
                                <path d="M27.7815 27.1277L22.8298 21.9295C24.103 20.4018 24.8005 18.4797 24.8005 16.4787C24.8005 11.8036 21.0321 8 16.4003 8C11.7684 8 8 11.8036 8 16.4787C8 21.1538 11.7684 24.9575 16.4003 24.9575C18.1391 24.9575 19.7962 24.4281 21.2129 23.4232L26.2023 28.6609C26.4108 28.8795 26.6913 29 26.9919 29C27.2764 29 27.5463 28.8905 27.7512 28.6914C28.1866 28.2686 28.2005 27.5675 27.7815 27.1277ZM16.4003 10.2118C19.8239 10.2118 22.6092 13.0231 22.6092 16.4787C22.6092 19.9344 19.8239 22.7456 16.4003 22.7456C12.9766 22.7456 10.1914 19.9344 10.1914 16.4787C10.1914 13.0231 12.9766 10.2118 16.4003 10.2118Z" fill="#171822"/>
                                <path d="M30 17.7445H35.163M30 22.9074H35.163M30 12.5815H35.163" stroke="#171822" strokeWidth="1.6" strokeLinecap="round" strokeLinejoin="round"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

function mapStateToProps(state) {
  return {
    city: state.defaultLocation,
    cityes: state.cityes
  }
}

export default connect(mapStateToProps, null)(Header);