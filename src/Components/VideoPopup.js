import React from 'react';

const hideVideoPopup = id => {
    const popup = document.getElementById(id);
    const layer = document.getElementById('main__bg-layer');

    popup.className = 'video-popup__hidden';

    const activeModal = document.getElementsByClassName("popup");
    
    if (activeModal.length > 0) {
        return;
    }

    setTimeout(() => {
        layer.className = 'main__bg-layer-hidden';
    }, 300);
}

const VideoPopup = props => {
    return (
        <div
            id={props.id}
            className={props.className}
        >
            <svg
                className="video-popup__active__close"
                width="20"
                height="20"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                onClick={() => hideVideoPopup(props.id)}
            >
                <path d="M19.7635 0.236752C19.448 -0.0787545 18.9365 -0.0787545 18.621 0.236752L0.236874 18.6208C-0.0786325 18.9364 -0.0786325 19.4479 0.236874 19.7634C0.394608 19.9212 0.601404 20 0.80816 20C1.01492 20 1.22167 19.9212 1.37945 19.7633L19.7635 1.37925C20.079 1.06378 20.079 0.552258 19.7635 0.236752Z" fill="white"/>
                <path d="M19.7633 18.6207L1.37912 0.236629C1.06366 -0.0788765 0.552097 -0.0788765 0.23663 0.236629C-0.0788766 0.552096 -0.0788766 1.06362 0.23663 1.37912L18.6208 19.7632C18.7785 19.921 18.9853 19.9999 19.1921 19.9999C19.3988 19.9999 19.6056 19.921 19.7633 19.7633C20.0787 19.4477 20.0787 18.9362 19.7633 18.6207Z" fill="white"/>
            </svg>
            <img className="video-popup__active__video" src={props.video} alt="video"></img>
            {
                props.cover
                    ? <img className="video-popup__active__cover" src={props.cover} alt="cover"></img>
                    : null
            }
            </div>
    )
}

export default VideoPopup;