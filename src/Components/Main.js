import React from 'react';
import Post from './Post';

const Main = props => {
	return (
		<div id="main" className="main">
	        {Object.values(props.posts).map((post, index) => {
				return (
					<Post
						key={index}
						posts={post}
						company={props.companies[index]}
						className={"post"}
						postId={index}
						withFooter={true}
						isHot={post.isHot}
						isNew={post.isNew}
						isPinned={post.isPinned}
						isGood={post.isGood}
						isBad={post.isBad}
						main={true}
					/>
				)
	        })}
        </div>
	)
}

export default Main;