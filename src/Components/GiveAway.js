import React from 'react';
import {withRouter} from 'react-router-dom';
import Logo from './Logo';
import Input from '../UI/Input';
import Canvas from './Canvas';

const GiveAway = props => {
    const post = Object.values(props.posts)[props.match.params.id];

    return (
        <div className="give-away">
            <div className="give-away__header">
                <p className="give-away__header-headliner"> give away</p>
            </div>
            <div className="give-away__logo-container">
                <Canvas />
                <Logo className="give-away__logo"/>
                <img className="give-away__logo-desktop" src={post.video} alt="video"></img>
            </div>
            <div className="give-away__text">
                <h3 className="give-away__headliner">{post.headliner}</h3>
                <p className="give-away__subheadliner">{post.subHeadliner}</p>
                <p className="give-away__description">{post.description}</p>
            </div>
            <form
                method="POST"
                action=""
                className="give-away__form">
                <Input
                    type={'text'}
                    placeholder={"ім'я та прізвище"}
                    className="give-away__input"
                />
                <Input
                    type={'tel'}
                    placeholder={"мобільний телефон"}
                    className="give-away__input"
                />
                <Input
                    type={'text'}
                    placeholder={"email"}
                    className="give-away__input"
                />
                <button className="give-away__button" type="submit">Надіслати</button>
            </form>
        </div>
    )
}

export default withRouter(GiveAway);