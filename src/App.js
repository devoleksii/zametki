import React from 'react';
import Header from '../src/Components/Header'
import './scss/index.scss';
import Feedback from './Components/Feedback';
import PostAllStocks from './Components/PostAllStocks';
import Popup from './Components/Popup';
import Main from './Components/Main';
import GiveAway from './Components/GiveAway';
import {Route, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

const popupIconMail =
    <svg
        width="60"
        height="60"
        viewBox="0 0 60 60"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <rect width="60" height="60" rx="30" fill="white"/>
        <path d="M31.9508 30.8683L34.7828 28.2921L39.4673 24.0483L30.0099 17.4641L20.5524 24.0483L25.237 28.2921L28.06 30.8683L29.5952 29.4921C29.8337 29.2715 30.1949 29.2803 30.4245 29.4921L31.9508 30.8683ZM19.2379 23.5548L29.5863 16.1679V16.159L29.5952 16.1501H29.6041L29.613 16.1412L29.6219 16.1323L29.6308 16.1235H29.6397L29.6486 16.1146L29.6575 16.1057L29.6664 16.0968H29.6752H29.6841V16.0884H29.693V16.0795H29.7108L29.7197 16.0707L29.7286 16.0618C29.7992 16.0351 29.8787 16.009 29.9493 16.0001H29.9582H29.9671H29.976H29.9848H29.9937H30.0026H30.0115H30.0204H30.0293H30.0382H30.0471H30.056H30.0649C30.1444 16.009 30.215 16.0351 30.2855 16.0618L30.2944 16.0707L30.3033 16.0795H30.3122H30.3211V16.0884H30.33L30.3389 16.0973H30.3478L30.3567 16.1062L30.3656 16.1151H30.3745L30.3834 16.124L30.3923 16.1329L30.4012 16.1418H30.4101L30.4189 16.1507L30.4278 16.1596V16.1685L40.7763 23.5553C40.9263 23.6698 41.0147 23.8466 41.0147 24.0494V39.3736C41.0147 39.7177 40.7412 40.0001 40.3972 40.0001H30.0099H19.6264H19.6086L19.5908 39.9912H19.5731H19.5647H19.5469H19.5291L19.5114 39.9823H19.5025H19.4852L19.4675 39.9734H19.4497L19.4408 39.9645H19.4235L19.4146 39.9556C19.1762 39.8672 19 39.6382 19 39.3731V24.0489C19 23.846 19.0967 23.6693 19.2379 23.5548ZM20.2439 25.4518V37.9707L24.3993 34.2033L27.1429 31.7065L24.3993 29.2186L20.2439 25.4518ZM39.7674 37.9707V25.4518L35.6209 29.2192L32.8685 31.7071L35.6209 34.2039L39.7674 37.9707ZM34.7828 35.121L30.0099 30.7894L25.237 35.121L21.2317 38.7472H30.0099H38.7792L34.7828 35.121Z" fill="#171822" stroke="#171822" strokeWidth="0.2"/>
        <rect opacity="0.4" x="0.5" y="0.5" width="59" height="59" rx="29.5" stroke="#BFC8D6"/>
    </svg>;

const App = props => {
    return (
      <div className="App">
        <div
          id="main__bg-layer"
          className="main__bg-layer-hidden"
        ></div>
        <Header />
        <Popup
            logo={popupIconMail}
            title={'Не втрачайте ваші піни.'}
            description={'Якщо Ви бажаєте отримувати усі новинки та актуальні акції, підпишіться на нашу розсилку.'}
            placeHolder={'email'}
            accept={'Отримати'}
            decline={'Скасувати'}
            type={'email'}
            id="popup-main"
            main={true}
            valid={true}
        />
        <Route path={"/"} component={() => (<Main posts={props.posts} companies={props.companies}/>)} exact/>
        <Route path={"/stocks"} component={() => (<PostAllStocks stocks={true} posts={props.posts} companies={props.companies}/>)} exact/>
        <Route path={"/stocks/:company"} component={() => (<PostAllStocks posts={props.posts} companies={props.companies}/>)} exact/>
        <Route path={"/new"} component={() => (<PostAllStocks new={true} posts={props.posts} companies={props.companies}/>)} exact/>
        <Route path={"/pinned"} component={() => (<PostAllStocks pinned={true} posts={props.posts} companies={props.companies}/>)} exact/>
        <Route path={"/giveAway/:id"} component={() => (<GiveAway posts={props.posts} companies={props.companies}/>)} exact/>
        <Route path={"/feedback"} component={Feedback} exact/>
      </div>
    );
  }

function mapStateToProps(state) {
  return {
    posts: Object.values(state.posts),
    companies: Object.keys(state.posts)
  }
}

export default withRouter(connect(mapStateToProps)(App));