const initialState = {
    posts: {
        epicentr: {
                logo: '/img/epicentr.png',
                company: 'Epicentr',
                location: 'gulliver',
                video: '/img/epicentr_video.png',
                cover: '/img/epicentr_cover.png',
                headliner: 'Будівельний безліміт подовжено!',
                subHeadliner: 'Будівельний безліміт в Епіцентрі подовжено!',
                description: 'Руйнуємо ціни. Купуй в Липні гіпсову плитку «Лофт» Класик з економією -30%. Пропозиція діє з 15.06.2019 до 31.07.2019',
                isHot: true,
                startDate: new Date(),
                isNew: false,
                isGood: false,
                isBad: false,
                isPinned: false
        },
        eldorado: {
                logo: '/img/eldorado.png',
                company: 'Eldorado',
                location: 'gulliver',
                video: '/img/eldorado_video.png',
                cover: '/img/eldorado_cover.png',
                headliner: 'Не втрачай часу та купуй телевізор в Eldorado у чисту розстрочку.',
                description: 'Вигідна пропозиція – «Супер Розстрочка» до 25 платежів. Замовляйте акційний товар в магазинах Eldorado на сайті, оформивши замовлення або зателефонувавши в call-центр за номером 0-800-502-2-55.',
                isHot: false,
                isNew: true,
                isGood: false,
                isBad: false,
                isPinned: false
        },
        eldorado1: {
                logo: '/img/eldorado.png',
                company: 'Eldorado',
                location: 'gulliver',
                video: '/img/eldorado_video.png',
                cover: '/img/eldorado_cover.png',
                headliner: 'Не втрачай часу та купуй телевізор в Eldorado у чисту розстрочку.',
                description: 'Вигідна пропозиція – «Супер Розстрочка» до 25 платежів. Замовляйте акційний товар в магазинах Eldorado на сайті, оформивши замовлення або зателефонувавши в call-центр за номером 0-800-502-2-55.',
                isHot: true,
                startDate: new Date(),
                isNew: false,
                isGood: false,
                isBad: false,
                isPinned: false
        },
        epicentr2: {
                logo: '/img/epicentr.png',
                company: 'Epicentr',
                location: 'gulliver',
                video: '/img/epicentr2_video.png',
                headliner: 'Не втрачай часу',
                subHeadliner: 'Не втрачай часу та купуй телевізор в Eldorado у чисту розстрочку.',
                description: 'Вигідна пропозиція – «Супер Розстрочка» до 25 платежів. Замовляйте акційний товар в магазинах Eldorado на сайті, оформивши',
                isHot: false,
                isNew: false,
                isGood: false,
                isBad: false,
                isPinned: false
        },
        haval: {
                logo: '/img/haval.png',
                company: 'Haval',
                location: 'gulliver',
                video: '/img/haval_video.png',
                headliner: 'HAVAL святкує свій перший День Народження в Україні.',
                subHeadliner: 'З нагоди першої річниці діють спеціальні вигідні ціни на лімітовану партію автомобілів HAVAL.',
                description: 'З 5 Липня по 31 Серпня Ви маєте можливість придбати стильний компактний кросовер HAVAL H2 із',
                isHot: false,
                isNew: false,
                isGood: false,
                isBad: false,
                isPinned: false
        },
        haval1: {
                logo: '/img/haval.png',
                company: 'Haval',
                location: 'gulliver',
                video: '/img/haval_video.png',
                headliner: 'HAVAL святкує свій перший День Народження в Україні.',
                subHeadliner: 'З нагоди першої річниці діють спеціальні вигідні ціни на лімітовану партію автомобілів HAVAL.',
                description: 'З 5 Липня по 31 Серпня Ви маєте можливість придбати стильний компактний кросовер HAVAL H2 із',
                isHot: false,
                isNew: false,
                isGood: false,
                isBad: false,
                isPinned: false
        },
    },
    cityes: [
        'Вінниця',
        'Дніпро',
        'Донецьк',
        'Житомир',
        'Запоріжжя',
        'Київ',
        'Краматорськ',
        'Луганськ',
        'Луцьк',
        'Львів',
        'Миколаїв',
        'Одеса',
        'Полтава',
        'Рівне',
        'Суми',
        'Тернопіль',
        'Ужгород',
        'Харків',
        'Херсон',
        'Хмельницький',
        'Черкаси',
        'Чернівці',
        'Чернігів'
    ],
    defaultLocation: {
        city: 'київ'
    }
};

export default function rootReducer(state = initialState, action) {
	switch(action.type) {
		case 'ADD_TO_PINNED':
            state.posts[action.payload.toLowerCase()].isPinned = true;
			return state;
		case 'REMOVE_FROM_PINNED':
            state.posts[action.payload.toLowerCase()].isPinned = false;
            return state;
        case 'MAKE_GOOD':
            state.posts[action.payload.toLowerCase()].isGood = !state.posts[action.payload.toLowerCase()].isGood;
            return state;
        case 'MAKE_BAD':
            state.posts[action.payload.toLowerCase()].isBad = !state.posts[action.payload.toLowerCase()].isBad;
            return state;
        case 'CHANGE_CITY':
            state.defaultLocation.city = action.payload;
            return state;
		default:
			return state;
	}
}